FROM mcr.microsoft.com/dotnet/core/sdk:3.0-stretch
#Install debugger
USER root
RUN apt-get update
RUN apt-get install wget  -y
RUN apt-get install apt-transport-https -y
RUN wget -q https://packages.microsoft.com/config/ubuntu/19.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN apt-get update
RUN apt-get install curl -y 
RUN apt-get install dotnet-sdk-3.0 -y
WORKDIR /src
COPY myMicroservice.csproj .
RUN dotnet restore
COPY . .
RUN dotnet publish -c release -o /app

FROM mcr.microsoft.com/dotnet/core/aspnet:3.0-stretch-slim
WORKDIR /app
ENTRYPOINT ["dotnet", "myMicroservice.dll"]