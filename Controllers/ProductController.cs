﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using myMicroservice.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace myMicroservice.Controllers {

    [Route ("[controller]")]
    public class ProductController : ControllerBase {
        private Product[] products = new [] {
            new Product { Id = "1", Sku = "PR01", Name = "Product1", Description = "Product1", StockLevel = 10 },
            new Product { Id = "2", Sku = "PR02", Name = "Product2", Description = "Product2", StockLevel = 10 },
            new Product { Id = "3", Sku = "PR03", Name = "Product3", Description = "Product3", StockLevel = 10 },
            new Product { Id = "4", Sku = "PR04", Name = "Product4", Description = "Product4", StockLevel = 10 },
            new Product { Id = "5", Sku = "PR05", Name = "Product5", Description = "Product5", StockLevel = 10 },

        };

        private readonly ILogger<ProductController> _logger;

        public ProductController (ILogger<ProductController> logger) {
            _logger = logger;
        }

        [HttpGet]
        public Product[] Get () {
            return products;
        }

        [HttpPut ("{id}")]
        public void Put (string id, [FromBody] Product product) {
            int index = 0;
            foreach (Product pr in products) {
                if (pr.Id == id) {
                    products.SetValue (product, index);
                }

            }
        }

    }
}