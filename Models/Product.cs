namespace myMicroservice.Models
{
    public class Product
    {
        public string Id { get; set; }
          public string Sku { get; set; }
  public string Name { get; set; }
   public string Description { get; set; }
      public int StockLevel { get; set; }

    }
}
